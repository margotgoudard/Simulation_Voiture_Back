package org.acme;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Voiture {
    private static final Logger logger = LoggerFactory.getLogger(Voiture.class);

    private static int positionX;
    private static int positionY;
    private static char direction = 'd'; // Initial direction to the right ('d')
    private static int carburant = 60; // Max carburant
    private static int compteurDeplacements = 0;
    private static int speed = 0; // Speed management
    private static List<Position> obstacles = new ArrayList<>();
    public static List<Position> boules = new ArrayList<>();

    private static ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();



    // Assuming you have a Position class defined somewhere
    static {
        // Example obstacles initialization
        obstacles.add(new Position(5, 5));
        obstacles.add(new Position(15, 15));
        boules.add(new Position(15, 15));
        boules.add(new Position(0, 5));
        initBoulesMovement();
        reinitialiser();
    } 

    private static void initBoulesMovement() {
        executorService.scheduleAtFixedRate(Voiture::deplacerBoulesVersVoiture, 0, 1, TimeUnit.SECONDS);
    }
    

    private static void deplacerBoulesVersVoiture() {
        // Iterate over each boule and move it towards the voiture
        for (Position boule : boules) {
            moveBouleTowardsVoiture(boule);
        }
    }
    
    private static void moveBouleTowardsVoiture(Position boule) {
        // Move the boule in the X direction towards the voiture
        if (boule.getX() < positionX) {
            boule.setX(boule.getX() + 1);
        } else if (boule.getX() > positionX) {
            boule.setX(boule.getX() - 1);
        }
    
        // Move the boule in the Y direction towards the voiture
        if (boule.getY() < positionY) {
            boule.setY(boule.getY() + 1);
        } else if (boule.getY() > positionY) {
            boule.setY(boule.getY() - 1);
        }
    }
    
    

    // Getters
    public static int getPositionX() {
        return positionX;
    }

    public static int getPositionY() {
        return positionY;
    }

    public static char getDirection() {
        return direction;
    }

    public static int getCarburant() {
        return carburant;
    }

    public static void setPositionX(int newPositionX) {
        positionX = newPositionX;
    }

    public static void setPositionY(int newPositionY) {
        positionY = newPositionY;
    }

    public static void setDirection(char newDirection) {
        direction = newDirection;
    }

    public static void setCarburant(int newCarburant) {
        carburant = newCarburant;
    }

    public static void deplacer(char touche) {
        if (carburant <= 0) {
            logger.info("La voiture est à court de carburant !");
            return;
        }

        // Simplified direction handling
        if (direction != touche) {
            setDirection(touche);
        } else {
            move(touche);
            checkForObstacleCollision();
        }
    }

    private static void move(char direction) {
        switch (direction) {
            case 'h': // haut
                moveUp();
                break;
            case 'b': // bas
                moveDown();
                break;
            case 'g': // gauche
                moveLeft();
                break;
            case 'd': // droite
                moveRight();
                break;
            default:
                logger.info("Touche non reconnue");
                break;
        }
    }

    private static void moveUp() {
        positionY -= 1;
        consommerCarburant();
    }

    private static void moveDown() {
        positionY += 1;
        consommerCarburant();
    }

    private static void moveLeft() {
        positionX -= 1;
        consommerCarburant();
    }

    private static void moveRight() {
        positionX += 1;
        consommerCarburant();
    }

    private static void checkForObstacleCollision() {
        boolean hitsObstacle = obstacles.stream().anyMatch(obstacle ->
            obstacle.getX() == positionX && obstacle.getY() == positionY);

        if (hitsObstacle) {
            if (speed > 2) {
                logger.info("Le véhicule est détruit à cause d'une collision à haute vitesse !");
            } else {
                logger.info("Collision avec un obstacle !");
            }
        }
    }

    private static void consommerCarburant() {
        compteurDeplacements++;
        if (compteurDeplacements >= 3) {
            carburant -= 1; // Consomme 1 litre de carburant
            compteurDeplacements = 0; // Réinitialise le compteur après la consommation
        }
    }
    

    public static void rechargerCarburant() {
        carburant = 60; // Recharge le carburant au maximum
    }

    public static void reinitialiser() {
        positionX = 10; 
        positionY = 15;
        direction = 'd'; // Default direction
        carburant = 60; // Default carburant
    }

   
}
