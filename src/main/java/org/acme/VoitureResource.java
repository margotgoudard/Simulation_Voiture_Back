package org.acme;

import jakarta.ws.rs.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jboss.resteasy.annotations.SseElementType;

import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.sse.Sse;
import jakarta.ws.rs.sse.SseEventSink;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Path("/voiture")
public class VoitureResource {
    private static final Logger logger = LoggerFactory.getLogger(Voiture.class);


    private static final String POSITION_X = "positionX";
    private static final String POSITION_Y = "positionY";
    private static final String DIRECTION = "direction";
    private static final String CARBURANT = "carburant";

    @GET
    @Produces(MediaType.SERVER_SENT_EVENTS)
    @SseElementType("application/json")
    public void positionStream(@Context SseEventSink eventSink, @Context Sse sse) {
        final ObjectMapper mapper = new ObjectMapper();
        final Runnable sendData = () -> {
            try {
                while (!Thread.currentThread().isInterrupted()) {
                    Map<String, Object> data = new HashMap<>();
                    data.put(POSITION_X, Voiture.getPositionX());
                    data.put(POSITION_Y, Voiture.getPositionY());
                    data.put(DIRECTION, Voiture.getDirection());
                    data.put(CARBURANT, Voiture.getCarburant());

                    // Ajouter les positions des boules
                    List<Map<String, Integer>> boulesPositions = Voiture.boules.stream()
                            .map(boule -> Map.of("x", boule.getX(), "y", boule.getY()))
                            .toList(); // Modified to use stream.toList()
                    data.put("boules", boulesPositions);

                    String json = mapper.writeValueAsString(data);

                    eventSink.send(sse.newEvent(json));
                    Thread.sleep(1000); // Mise à jour chaque seconde
                }
            } catch (Exception e) {
                Thread.currentThread().interrupt();
            }
        };

        new Thread(sendData).start();
    }

    @POST
    @Path("/deplacer/{direction}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Map<String, Object> deplacerVoiture(@PathParam("direction") String direction) {
        Voiture.deplacer(direction.charAt(0));
        Map<String, Object> response = new HashMap<>();
        response.put(POSITION_X, Voiture.getPositionX());
        response.put(POSITION_Y, Voiture.getPositionY());
        response.put(DIRECTION, Voiture.getDirection());
        response.put(CARBURANT, Voiture.getCarburant());
        logger.info("Réponse envoyée: " + response);
        return response;
    }

    @POST
    @Path("/recharger")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Map<String, Object> rechargerVoiture() {
        Voiture.rechargerCarburant();
        Map<String, Object> response = new HashMap<>();
        response.put(POSITION_X, Voiture.getPositionX());
        response.put(POSITION_Y, Voiture.getPositionY());
        response.put(DIRECTION, Voiture.getDirection());
        response.put(CARBURANT, Voiture.getCarburant());
        logger.info("Carburant rechargé et réponse envoyée: " + response);
        return response;
    }

    @GET
    @Path("/boules")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Map<String, Object>> getBoules() {
        List<Map<String, Object>> boulesList = new ArrayList<>();

        for (Position boule : Voiture.boules) {
            Map<String, Object> bouleData = new HashMap<>();
            bouleData.put("x", boule.getX());
            bouleData.put("y", boule.getY());
            boulesList.add(bouleData);
        }

        return boulesList;
    }

    @POST
    @Path("/reinitialiser")
    public Response reinitialiserJeu() {
        Voiture.reinitialiser();
        return Response.ok().build();
    }
}
